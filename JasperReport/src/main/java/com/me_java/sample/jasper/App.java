package com.me_java.sample.jasper;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class App extends JFrame {
	private static final long serialVersionUID = 1L;

	// To show source code https://gitlab.com/mj17/jasperreportsample

	private JPanel pane;
	private JButton btnPrintJson = new JButton("PrintWithJson");
	private JButton btnPrintSql = new JButton("PrintWithSql");

	public App() {
		setTitle("Jasper Report");
		setLocation(500, 200);
		setSize(150, 100);
		setResizable(false);

		prepareLayout();
		setComponentAction();

		setVisible(true);
	}

	public void prepareLayout() {
		pane = new JPanel();
		pane.setBackground(Color.decode("#e29ad6"));
		pane.add(btnPrintJson);
		pane.add(btnPrintSql);
		add(pane);
	}

	public void setComponentAction() {
		btnPrintJson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					// need to add 'jasperreports' dependency
					
					File jsonFile = new File("report/data/person.json");
					JasperReport jrForJson = JasperCompileManager
							.compileReport("report/template/ReportWithJson.jrxml");
					JasperPrint jpForJson = JasperFillManager.fillReport(jrForJson, null, new JsonDataSource(jsonFile));
					JasperViewer jvForJson = new JasperViewer(jpForJson);
					jvForJson.setVisible(true);
				} catch (JRException | FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
		
		btnPrintSql.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					
					// need to add 'jasperreports' dependency

					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost/me_and_java?user=root&password=root");
					JasperReport jrForSql = JasperCompileManager.compileReport("report/template/ReportWithSql.jrxml");
					JasperPrint jpForSql = JasperFillManager.fillReport(jrForSql, null, con);
					JasperViewer jvForSql = new JasperViewer(jpForSql);
					jvForSql.setVisible(true);
				} catch (JRException | ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void main(String[] args) {
		new App();
	}

}
